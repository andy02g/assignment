import React, { Component } from "react";
import axios from "axios";
import { BootstrapTable, TableHeaderColumn, ExportCSVButton } from 'react-bootstrap-table';
import CSVReader from "react-csv-reader";
import * as emailjs from 'emailjs-com'


import { Button, FormFeedback, Form, FormGroup, Label, Input,Table} from 'reactstrap'
import './converter.css';

class Converter extends Component {

    constructor(props){
        super(props);

        this.state = {
            feedback: '',
            name: 'Name',
            email: 'email@example.com',
            result: null,
            fromCurrency: "USD",
            toCurrency: "GBP",
            amount: 1,
            fromamount:'',
            usdAmount: '',
            currencies: [],
            indianValue:[],
            dollorValue:[],
            name: '',
            email: '',
            subject: '',
            message: '',
            newValues: {}

        };

        // this.handleChange = this.handleChange.bind(this);

        this.handleChange1 = this.handleChange1.bind(this);
	    this.handleSubmit = this.handleSubmit.bind(this);
        
        
    }
  

    componentDidMount() {
        
        let inverterkpiAPI = "https://api.openrates.io/latest"
        axios.get(inverterkpiAPI, { withCredentials: true })
            .then(res => {
                console.log("==============",res)
            })

    }

    // // Initializes the currencies with values from the api
    // componentDidMount() {
    //     axios
    //         .get("http://api.openrates.io/latest")
    //         .then(response => {
    //             // Initialized with 'EUR' because the base currency is 'EUR'
    //             // and it is not included in the response
    //             const currencyAr = ["EUR"]
    //             for (const key in response.data.rates) {
    //                 currencyAr.push(key)
    //             }
    //             this.setState({ currencies: currencyAr.sort() })
    //         })
    //         .catch(err => {
    //             console.log("Opps", err.message);
    //         });
    // }

    // Event handler for the conversion
    convertHandler  = ()  => {

        console.log("clicked====");

        console.log("fromamount",this.state.fromamount)
        console.log("this.state.usdAmount",this.state.usdAmount)

        const from_currency = this.state.fromamount;
        
	    const to_currency = this.state.usdAmount;
	
	    fetch(`https://api.exchangerate-api.com/v4/latest/${from_currency}`)
            .then(res => res.json())
            .then(res => {
                console.log("res====",res)
            // const rate = res.rates[to_currency];
            // rateEl.innerText = `1 ${from_currency} = ${rate} ${to_currency}`
            // to_ammountEl.value = (from_ammountEl.value * rate).toFixed(2);
        })
    
        // if (this.state.fromCurrency !== this.state.toCurrency) {
        //     axios
        //         .get(`http://api.openrates.io/latest?base=${this.state.fromCurrency}&symbols=${this.state.toCurrency}`)
        //         .then(response => {
        //             const result = this.state.amount * (response.data.rates[this.state.toCurrency]);
        //             this.setState({ result: result.toFixed(5) })
        //         })
        //         .catch(err => {
        //             console.log("Opps", err.message);
        //         });
        // } else {
        //     this.setState({ result: "You cant convert the same currency!" })
        // }
    };

    // Updates the states based on the dropdown that was changed
    selectHandler = (event) => {
        if (event.target.name === "from") {
            this.setState({ fromCurrency: event.target.value })
        }
        if (event.target.name === "to") {
            this.setState({ toCurrency: event.target.value })
        }
    }

    // handleChange = (event) =>  {
    //     console.log("event",event.target.value)
    //     this.setState({fromamount: event.target.value});
    //   }

      handleChange1 = (param, e) => {
        this.setState({ [param]: e.target.value })
      }

     
    
      handleSubmit (event) 
      {

        event.preventDefault()
        const { name, email, subject, message } = this.state

        let templateParams = {
        from_name: email,
        to_name: 'klemcaanand@gmail.com',
        subject: subject,
        message_html: message,
        }
        emailjs.send(
        'gmail',
        'template_mbUs2jAR',
        templateParams,
        'user_P9D1wOnsEVEUler201YAs'
        )
     this.resetForm()
       
      }

      resetForm() {
        this.setState({
          name: '',
          email: '',
          subject: '',
          message: '',
        })
      }
    



    USDChange = (event) => {
        console.log("event",event.target.value)
        this.setState({usdAmount: event.target.value});
    }

    handleForce = (data) => {
        console.log(data);

        var dollorValue1 = []
        var indianValue1 = []



        const arrAvg = dollorValue1 => dollorValue1.reduce((a,b) => a + b, 0) / dollorValue1.length

        console.log(arrAvg + " arrAvg");

        data.map(function(item,index){


            
           console.log("item====",item)
           console.log("index====",index)



           indianValue1.push(item)
          

        //    this.setState({indianValue: item});

            var dollars = item / 64;
            console.log(dollars + " Dollars");

            dollorValue1.push(dollars)
            

            // this.setState(dollorValue: dollorValue1);

            // this.setState({dollorValue: dollars});
           

        })

        this.setState({dollorValue: dollorValue1});
        this.setState({indianValue: indianValue1});


        var newValues = JSON.stringify(dollorValue1)

        this.setState({newValues: newValues});

        console.log("dddddddddddddddddddddd=======",newValues)

        

        

      
        

       
        
      };

    render() {

        var products = [{
            id: 1,
            name: "Product1",
            price: 120
        }, {
            id: 2,
            name: "Product2",
            price: 80
        }];

      
        console.log("-=--==-=-=->",this.state.newValues)

       
        return (

          
            <div className="Converter">
                <h2><span>Currency </span> Converter  </h2>
                <div className="Form">
                    {/* <input
                        name="amount"
                        type="text"
                        value={this.state.amount}
                        onChange={event =>
                            this.setState({ amount: event.target.value })
                        }
                    /> */}

                    <p>IND Currency</p> 

                    {/* <input 
                    type="text" 
                    value = {this.state.fromamount} 
                    id="from_ammount" 
                    onChange={event =>
                        this.setState({ fromamount: event.target.value })
                    }
                    /> */}


                    <input type="text" value={this.state.fromamount} onChange={this.handleChange} />



                    <p>USD Currency</p> 

                    <input type="text" value={this.state.usdAmount} onChange={this.USDChange} />               
                  
                    <button onClick={this.convertHandler}>Convert</button>

                </div>
                {this.state.result && 
                    <h3>{this.state.result}</h3>
                }

            <div className="container" >
                <CSVReader
                cssClass="react-csv-input"
                label="Select CSV "
                onFileLoaded={this.handleForce}
                />
                
            </div>

            <Table dark>
      <thead>
        <tr>
          <th>#</th>
          {/* <th>Indian Value</th> */}
          <th>Dollars Value</th>
        </tr>
      </thead>
      <tbody>
      {this.state.dollorValue.map((rowData, index) => (
               
                // this.state.dollorValue.map((rowData1, index) => (
        <tr>
           <th scope="row">{index + 1}</th>
                     <td>{rowData}</td>
                     {/* <td>{rowData1}</td> */}
        </tr>
         
        // ))
        
        ))}

      {/* {this.state.dollorValue.map((rowData1, index) => (
        <tr>
           <th scope="row">{index + 1}</th>
                     <td>{rowData1}</td>
        </tr>
        ))} */}
      </tbody>
    </Table>


           





    
            {/* <TableHeaderColumn isKey dataField='id' width='250' >Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name' width='250' >Product Name</TableHeaderColumn>
             <TableHeaderColumn dataField='price' width='250'>Product Price</TableHeaderColumn> */}
           
          <h1 className="p-heading1">Get in Touch</h1>
          <Form onSubmit={this.handleSubmit.bind(this)}>
            <FormGroup controlId="formBasicEmail">
              <Label className="text-muted">Email address</Label>
              <Input
                type="email"
                name="email"
                value={this.state.email}
                className="text-primary"
                onChange={this.handleChange1.bind(this, 'email')}
                placeholder="Enter email"
              />
            </FormGroup>
<FormGroup controlId="formBasicName">
              <Label className="text-muted">Name</Label>
              <Input
                type="text"
                name="name"
                value={this.state.name}
                className="text-primary"
                onChange={this.handleChange1.bind(this, 'name')}
                placeholder="Name"
              />
            </FormGroup>
<FormGroup controlId="formBasicSubject">
              <Label className="text-muted">Subject</Label>
              <Input
                type="text"
                name="subject"
                className="text-primary"
                value={this.state.subject}
                onChange={this.handleChange1.bind(this, 'subject')}
                placeholder="Subject"
              />
            </FormGroup>
<FormGroup controlId="formBasicMessage">
              <Label className="text-muted">Message</Label>
              <Input
                type="textarea"
                name="message"
                className="text-primary"
                value={this.state.newValues}
                onChange={this.handleChange1.bind(this, 'message')}
              />
            </FormGroup>
<Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
      
            </div>

          

           
        );
    }
}

export default Converter;
